# ICALEPCS 2023

Papers for ICALEPCS 2023 - International Conference on Accelerator and Large Experimental Physics Control Systems

## Usage

In order to make a contribution, please create a branch and a merge request with all the changes you think are important to do. 

We use latex to write papers. There are 2 folders, for 2 papers (one oral, one poster). 
The monitoring folder contains the paper for the prometheus architecture and the first draft of the poster (in power point).
The ska-tango-operator folder contains the paper the k8s extension to TANGO. 

## Windows

I use tex studio: https://www.texstudio.org/
